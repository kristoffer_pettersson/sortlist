package Lista;

import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Person agda = new Person("Agda", 20);
        Person gunnar = new Person("Gunnar", 20);
        Person beda = new Person("Beda", 20);
        Person per = new Person("Per", 20);
        Person lisa = new Person("Lisa", 20);
        Person plutten = new Person("Plutten", 20);
        Person klimpen = new Person("Klimpen", 20);
        Person lillen = new Person("Lillen", 20);
        Person minna = new Person("Minna", 20);
        Person minna2 = new Person("Minna", 20);
        Person pelle = new Person("Nelle", 20);

        Lista persons = new Lista();

        Stream.of(agda, gunnar, beda, per, lisa, plutten, klimpen, minna2, lillen, minna, pelle).forEach(person -> {
            persons.addPersons(person);
        });

        for (Object person : persons.sortPersons()) {
            System.out.println(person);
        }
        System.out.println();

        System.out.println(persons.listOfLetter("P"));
        System.out.println();

        System.out.println(persons.getHighestValueOfFirstLetter());
    }
}
