package Lista;

import java.util.*;
import java.util.stream.Collectors;

public class Lista {
    private List<Person> persons = new ArrayList<>();

    public void addPersons(Person person) {
        if (person.getName().equals("") || person.getAge() <= 0) {
            throw new NullPointerException("Invalid Input");
        }
        persons.add(person);
    }

    public List listOfLetter(String letter) {
        return persons.stream()
                .filter(e -> e.getName().startsWith(letter))
                .collect(Collectors.toList());
    }

    public List sortPersons() {
        return persons.stream()
                .sorted(Comparator.comparing(Person::getName))
                .collect(Collectors.toList());
    }

    private Map<Character, Long> countByFirstLetter() {
        return persons.stream()
                .collect(Collectors.groupingBy(p -> p.getName().charAt(0), Collectors.counting()));
    }

    public Map<Character, Long> getHighestValueOfFirstLetter() {
        Optional<Map.Entry<Character, Long>> maxValue = countByFirstLetter().entrySet().stream()
                .max(Map.Entry.comparingByValue());

        return countByFirstLetter().entrySet().stream()
                .filter(x -> x.getValue().equals(maxValue.get().getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    public List<Person> getPersons() {
        return persons;
    }

}

//lösning
/*
persons.stream()
               .collect(Collectors.groupingBy(p -> p.getName().charAt(0)))
               .forEach((person, count) -> System.out.println(person + " " + count.size()));*/


/*
return persons.stream()
        .collect(Collectors.groupingBy(p -> p.getName().charAt(0), Collectors.counting()));*/


/*
System.out.println(tmpArray.entrySet().stream().filter(x -> x.getValue() == 3)
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
*/


// return countByFirstLetter().entrySet().stream().max(Map.Entry.comparingByValue());

/**
 * true solution
 * public Map<Character, Long> getHighestValueOfFirstLetter() {
 * System.out.println(countByFirstLetter());
 * <p>
 * Optional<Map.Entry<Character, Long>> maxValue = countByFirstLetter().entrySet().stream()
 * .max((o1, o2) -> o1.getValue().compareTo(o2.getValue()));
 * <p>
 * return countByFirstLetter().entrySet().stream()
 * .filter(x -> x.getValue().equals(maxValue.get().getValue()))
 * .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
 * }
 */